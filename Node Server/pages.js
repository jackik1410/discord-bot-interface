const fs = require('fs');
const publicIP = require('public-ip');

let port = "80";
// import {html, render} from 'lit-html';



const http = require('http');
const mime = require('mime');

//Posting to ddnss to update the ip, just in case;
// http.get("http://www.ddnss.de/upd.php?key=d482ae64c28bc5a367c77ca8df798fd9&host=jackik.ddnss.org", (res)=>{
//   console.log("Loading discord bot website, ddnss ip update returned: " + res.statusMessage);
// });




// var customHtmlPage = "<h1> STILL LOADING </h1>";
// fs.readFile('./index.html', "utf-8", (err, data)=>{
//     if(err) winston.error(err);
//     customHtmlPage = ""+data; //forcing type to string
// });


//process.cwd()
//create a server object:
// console.log("Actually starting server");

function serveFunction(req, res) {
    console.log("Trying to serve client");
    console.log(req.url);
    try {
        if (req.url === "/") {
            req.url="/index.html";
            // res.writeHead("307");
            // // res.write("\/index.html");
            // res.end();
            // return;
            // // req.url = "\/index.html";
        }
        let hasFileExtension = req.url.match(/(.){1,}(\.)(.){1,4}/i); //checks if something with a file extension is being requested
        if (!hasFileExtension) {
            req.url+=".html";
        }

        // fs.readFile("E:/CodeStuff/html/Node Server"+"/page"+req.url, "utf-8", (err, data)=>{
        fs.readFile(__dirname+"\/..\/page\/"+req.url, "utf-8", (err, data)=>{
            if(err){
                console.log(err);
                // winston.error(err);
                res.writeHead("404");
                res.write("File doesn't exist or is inaccessible");
                res.end(); //end the response
            } else {
                let mimetype = mime.getType(req.url.split(".").pop());
                // console.log(mimetype, data);
                res.setHeader("Content-Type", mimetype);
                res.write(data);
                // console.log(data);
                // res.writeHead("200");
                res.end(); //end the response
            }
        });

    } catch (err) {
        console.log(err);
    }
}

let os = require('os');
let networkInterfaces = os.networkInterfaces();
// console.warn(networkInterfaces);

let httpServerObject = {};

async function loadConnections(){
  httpServerObject = {};

  //looks for public IPs and hosts servers on them
  await ["v4", "v6"].forEach(async v => {
      let ip = await publicIP[v]();
      if (Object.keys(httpServerObject).includes(ip)) return;
      httpServerObject[ip] = http.createServer(serveFunction);
      httpServerObject[ip].listen(port);
      console.log("public IP: "+ip);
  });

  Object.entries(networkInterfaces).forEach(([key, value])=>{
      // if (["Hamachi", "Teamviewer"].includes(key)) return;
      value.forEach(singularConnection => {
          let ip = singularConnection.address;
          if (singularConnection.internal === true) return;
          if (Object.keys(httpServerObject).includes(ip)) return;
          if (singularConnection.family.match(/IPv(4|6)/)) {
              httpServerObject[ip] = (http.createServer(serveFunction));
          }
          console.warn(`hosting server over ${key}: `+ singularConnection.family+ ": " + ip);
      });
  });
  // console.log(Object.keys(httpServerObject));
}

function startServing(){
    loadConnections();
    Object.entries(httpServerObject).forEach(([ip, server])=>{
        server.listen(port, ip);
    });
    console.log("Now hosting on port "+port+" over all interfaces.");
    // console.log(Object.keys(httpServerObject));
}

startServing();

module.exports = {
    "httpServerObject":httpServerObject,
    "stopServing": function(){
        Object.entries(httpServerObject).forEach(([ip, server])=>{
            server.close();
        });
    },
    "startServing": startServing
};
