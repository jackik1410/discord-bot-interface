const {db, winston, reboot} = require("./logger.js");
const fs = require("fs");
// var client = require('./client.js');

var ConsoleCommands = { // commands defined just for CLI usage, primarily for debug and development
    "test": function (){
        console.log('works...\n fine!');
    },
    "eval": function (line, command){ // not actualy needed, but will keep for now
        try {
            console.log(eval(line.trim().slice(command.length +1)));
        } catch (err) {console.log(err);}
    },
    "restart": function (){
        reboot();
    },
    "": function (){

    }
}


const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    prompt:''
});
rl.on('line', (line) => {
    try {
        // winston.silly(`CLI input: "${line}"`); // just in case something breaks, will be loged in the files
        var args = line.trim().split(/ +/g);
        var command = args.shift().toLowerCase();
        if (ConsoleCommands[command] != null) {
            ConsoleCommands[command](line, command, args);
        } else {
            // standart commandhandler
            try {
                console.log(eval(line));
            } catch (err) {console.log(err);}
        }

    } catch (err) {
        winston.error(err);
    } finally {
        //nothing
    }
    rl.prompt();
});
