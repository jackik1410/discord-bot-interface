const winston = require('winston');
// const Transport = require('winston-transport'); //currently not used, but might add
const fs = require("fs");


winston.format.combine(
    winston.format.colorize(),
    winston.format.json()
);
winston.remove(winston.transports.Console);
let moment = require('moment');
winston.add( new winston.transports.File({ filename: `logs/${moment().format('YY.MM.DD')}.log`, level: 'debug', format: winston.format.combine(
    winston.format.timestamp(),
    // winston.format.simple(),
    winston.format.printf(info => `${info.timestamp} [${info.level}]: ${info.message}`),
)}));
winston.add(new winston.transports.Console({level: 'silly',
    format : winston.format.combine(
        winston.format.colorize(),
        winston.format.timestamp(),
        // winston.format.simple(),
        winston.format.printf(info => `${info.timestamp} [${info.level}]: ${info.message}`),
    )
}), {
  // colorize: true,
  // level: 'silly'
});
// winston.level = 'info';
  // new winston.transports.File({ filename: 'error.log', level: 'error' });
  // new winston.transports.File({ filename: 'combined.log' });
// var client = require('./client.js');
// client.on('ready', () => {
//   class CustomTransport extends Transport {
//     constructor(opts) {
//       super(opts);
//       // Consume any custom options here. e.g.:
//       // - Connection information for databases
//       // - Authentication information for APIs (e.g. loggly, papertrail,
//       //   logentries, etc.).
//     }
//     log(info, callback) {
//       // Perform the writing to the remote service
//       var message = `${info.level}: ${info.message}`;
//
//       var VWG = client.guilds.get('388765646554398734');
//       var jacksID = '274303955314409472';
//       OPs.RealAdmins.forEach(async (admin) => {
//         client.fetchUser(admin).then(user => {
//
//           if (info.level == 'error') {
//
//             if ( VWG.presences.has(jacksID) || user.id == jacksID) { //if i'm online, annoy me only
//               message = `${user.toString()} ` + message;
//             }
//           }
//
//           user.send(message);
//         });
//       });
//       // setImmediate(() => {
//       //   this.emit('logged', info);
//       // });
//       callback();
//     }
//   }
//   // const transport = new CustomTransport();
//
//   // winston.add( new CustomTransport);
//   winston.add( new CustomTransport());
// });


// DataBase
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const adapter = new FileSync('./dbs/db.json');
const db = low(adapter);



function reboot(){
    const { spawn } = require('child_process');
    const subprocess = spawn(process.argv[0], [`../bot.js`], {
        detached: true,
        // stdout: process.stdout,
        // stdio: 'inherit'
        stdio: ['ignore', 'ignore', 'ignore']
    });
    if (subprocess == undefined) {
        winston.info("Couldn't restart bot");
        return;
    }
    if (subprocess.connected) {
        winston.info('disconnecting subprocess');
        subprocess.disconnect();
    }
    subprocess.unref();
    process.exit();
}

module.exports = {
    "winston": winston,
    "db": db,
    "reboot": reboot,
    // "abortrestart": abortRestart,
};
