var {winston} = require('./logger.js');
winston.info("Logger initialized");

const fs = require('fs');

// require('lit-html');
require('./consoleinput.js');
winston.info("CLI initialized");


// import {html, render} from 'lit-html';

require("./pages.js");

winston.verbose("finished with setup!");
