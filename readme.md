Source at: https://gitlab.com/jackik1410/discord-bot-interface

1 What is this?
This is an interface for bot accounts that aims to make using one as simple as possible, by making fundamental actions such as writing and reading messages as similar to the regular Discord experience. The primary use-case for this is server management, such as kicking and banning, but this will be heavily expanded upon, with the future addition of a javascript console, loadable JSON files to populate menus (drop-down and context ones) as well as some other things.

2 Installation
(inside the "Node Server" folder)
The project comes with a batch file, namely the autorun.bat. For windows, installing it, just means installing NodeJS first and then just running the file and pressing "I" when prompted, which will begin installing the necessary modules. After that, running it is just the same, starting the batch file and pressing "R" for run instead.

3 Current Features
3.1 Interface
The interface is obviously and intentionally leaned against the normal Discord look, with liberties taken along the way. There was no need to match it perfectly, as quite a few features, such as looking at profiles, don’t even exist for bot accounts. The decision was taken to only have it approximate in style and most importantly layout, for easy interaction. Bounding boxes for users will remain as is, to make picking them up easier (drag-drop).

3.1.1 Context menus
Context menus are populated using JSON objects, which make them easy to add to and change quickly. This also allows for them to be added to other types of objects. Right now, only User elements have context actions, but that will be expanded to servers, channels and individual messages.

3.1.2 Drag and Drop
Right now only User elements as well as inline User elements are draggable, but more will be added in the future.

3.2 Client
The client object is created and initialized in the global-scope, directly in the HTML file. This is so that there is direct and easy access to every thing Discord right from the web console. Even when a custom and own console is added, this will most probably remain as is.

4 Limitations of Bot accounts and the minified web version
Sadly, Discord has intentionally limited accessible features for bot accounts. While it would be possible to use a "regular" user account, doing so, would violate the Discord ToS.
Discord Bots have no access to DM history, so every time the site is reloaded, a new DM channel has to be created and all previous interaction will remain invisible to the bot. Furthermore, the minified web version is intended for web usage, instead of node, which means that there is no inherit way of handling audio, as there is no access to FFmpeg.

5 Plans for the future
  -Add more actions to menus
  -Add more menus to objects
  -Add more object to screens
  -Get a system running to minify and concat the many .js files to make serving faster (loading times have been impacted quite a lot by separating functions into smaller files.)
  -Work out ways to use the interface more, such as having the server provide node features the web version lacks, such as audio capability.
  -work over the layout and make the app compatible and usable with very large and very small displays sizes. 4k works but phones not very much;

6 Filesystem
The project includes both the page and files required for it, as well as a server to host them. "page" comprises the site, while "Node Server" is the server.
