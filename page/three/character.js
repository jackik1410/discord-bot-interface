import {} from "https://cdnjs.cloudflare.com/ajax/libs/three.js/108/three.js";
import {camera} from "./scene.js";

class characterPart extends THREE.Mesh {
    constructor( geometry, material, xpos, ypos, zpos, xrot, yrot, zrot) {
        super(
          geometry||new THREE.CubeGeometry(1,1,1),
          material||new THREE.MeshBasicMaterial()
        );

        this.offset = {
          x:(xpos||0), y:(ypos||0), z:(zpos||0), rot:{x:(xrot||0), y:(yrot||0), z:(zrot||0)}
        };
    }
}




let character = {
    parts: [new characterPart(null, null, 0, 2, 0)]
};




export {character};
