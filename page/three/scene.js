import {} from "https://cdnjs.cloudflare.com/ajax/libs/three.js/108/three.js";
import { TrackballControls } from 'https://threejs.org/examples/jsm/controls/TrackballControls.js';

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );

var renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

export {renderer, scene, camera};

//now is actual set up
// envMap: mirrorCamera.renderTarge
var dirt = new THREE.MeshBasicMaterial({
    // color: 0x435243,
    envMap: camera.renderTarget
});
dirt.mapping = THREE.CubeReflectionMapping;
scene.add(new THREE.Mesh( new THREE.PlaneGeometry(5, 5, 0), dirt));


var geometry = new THREE.BoxGeometry( 1, 1, 1 );
var material = new THREE.MeshBasicMaterial( { color: 0xf2ff00 } );
var cube = new THREE.Mesh( geometry, material );
scene.add( cube );


let sphereShape = new THREE.SphereGeometry(1, 3, 8, 0, 6.3, 0, 6.3);
let projectile = new THREE.Mesh( sphereShape, material );

scene.add( projectile );
projectile.position.x = 3;

camera.position.z = 5;

import {character} from "./character.js";
character.parts.forEach(part=>{
    camera.add(part);
});


var animate = function () {
    requestAnimationFrame( animate );
    controls.update();

    //update Character as well
    // if(character && character.parts) character.parts.forEach(part=>{
    //     part.position.x = camera.position.x + part.offset.x;
    //     part.position.y = camera.position.y + part.offset.y;
    //     part.position.z = camera.position.z + part.offset.z;
    //
    //     part.rotation.x = camera.rotation.x + part.offset.rot.x;
    //     part.rotation.y = camera.rotation.y + part.offset.rot.y;
    //     part.rotation.z = camera.rotation.z + part.offset.rot.z;
    // });
    // console.log(camera.position);
    // console.log(character.parts[0].position);



    //clunky animations here:
    cube.rotation.x += 0.01;
    cube.rotation.y += 0.001;

    projectile.rotation.x += 0.01;
    projectile.rotation.y += 0.001;

    renderer.render( scene, camera );
};




let controls = new TrackballControls( camera, renderer.domElement );
		controls.rotateSpeed = 6.0;
		// controls.zoomSpeed = 1.2;
		// controls.panSpeed = 0.8;
		controls.noZoom = true;
		controls.noPan = true;
		controls.staticMoving = false;
		controls.dynamicDampingFactor = 0.3;
		controls.keys = [ 65, 83, 68 ];
		// controls.addEventListener( 'change', renderer.render );

window.onresize = function () {
  	camera.aspect = window.innerWidth / window.innerHeight;
  	camera.updateProjectionMatrix();
  	renderer.setSize( window.innerWidth, window.innerHeight );
};
animate();
