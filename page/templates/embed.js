import {html} from "https://unpkg.com/lit-html?module";

function getId(url) {
    var regExpYoutube = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
    var match = url.match(regExpYoutube);

    if (match && match[2].length == 11) {
        return match[2];
    } else {
        return 'error';
    }
}
let regExpTwitch = /(.){0,12}(twitch.tv\/)(.)+/g;

let videoWidth = `560`;
let videoHeight = `315`; // preserves 16/9 ratio, but there may be a better alternative, I just don't know of any
const videoFrame = (url)=>{
    if (url.match(/(.){0,12}(youtube.com\/|youtu.be\/)(.)+/g)) {
      url = "https://www.youtube.com/embed/"+getId(url);
    } else if (url.match(regExpTwitch)) {
      url = "https://player.twitch.tv/?"+url.match(regExpTwitch)[3];
    } else {
      console.warn("unknown video source, don't know how to parse! " + url);
    }
    return html`
      <div class="embedVideo">
        <div></div>
        <iframe src="${url}" width="${videoWidth}" height="${videoHeight}" allowfullscreen="true"></iframe>
      </div>
    `;
}

const Placeholder = html`
    <em> PLACEHOLDER <em>
`;

export default (embed) => {
    return html`
        <div class="embed">
        Embed of type ${embed.type} here, to be done...
        All embeds are still heavily WIP
        ${function(){
          // console.log(embed.type);
          switch (embed.type) {
            case 'video':
              return videoFrame(embed.url);
              break;
            case 'link':
              return html`
                <a href="${embed.url}">${embed.url}<a>
              `;
              break;
            case 'rich':
              return html`
                <div class="embed">
                </div>
              `;
              break;
            case 'image':
              return Placeholder;
              break;
            case 'article':
              return Placeholder;
              break;
            default:
              console.warn(embed.type);
              // return JSON.stringify(embed);
          }
        }()}
        </div>
    `;
}
