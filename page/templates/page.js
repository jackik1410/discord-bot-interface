'use strict';
import {html} from "https://unpkg.com/lit-html?module";

import * as interaction from "../interaction.js";

let defaultAvatar = "https://cdn.discordapp.com/embed/avatars/0.png";

import elements from "./elements.js";

import {renderAll} from "./render.js";

let pseudoGuild = {
    id: "Friends and Users",
    name: "Users",
    iconURL: defaultAvatar,
    channels: false,
};

function listguilds(client, guilds, focussedguild){
    if(!guilds || guilds.size == 0) return;
    let guildArray = [pseudoGuild].concat(guilds.array());
    return guildArray.map(guild => elements.server(guild, focussedguild));
}


let statusEnum = ["online", "idle", "dnd", "offline"];
function sortUsersByStatus(one, two){
    return statusEnum.indexOf(one.presence.status) - statusEnum.indexOf(two.presence.status);
}

function sortUsersByLastMessage(one, two){
    if(one.lastMessage){
        if(two.lastMessage){
            return one.lastMessage.createdTimestamp - two.lastMessage.createdTimestamp;
        } else {
            return 1;
        }
    } else {
        if(two.lastMessage){
            return -1;
        } else {
            return one.name - two.name;
        }
    }
}


const searchbox= ()=>html``;

const userlistObject = (userArray, options) => {
    return html`
        <div class="users">
          <div class="searchbox">
            <textarea class="searchbox" @keydown=${e=>{
              renderAll(client, focussed);
            }} placeholder="search here"></textarea>
            <div class="tooltip">
              This doesn't work correctly yet, but will be able to search all html elements within a div. (ideally)
            </div>
          </div>
          <div>
            ${userArray.map(user => elements.user(user, options))}
          </div>
        </div>
    `;
}

const userlist = (client, focussed, sortFunction, options)=>{
    if(focussed.guild && focussed.guild.id === "Friends and Users") {
        return userlistObject(client.users.array(), options);
    }
    if (focussed.channel) {
        if (focussed.channel.type != "text") {
            return focussed.guild.members.array()
              .sort(sortFunction||sortUsersByStatus)
              .map(member => elements.user(member, options));
        }
        let userArray = focussed.channel.members.array().sort(sortFunction||sortUsersByStatus);

        return userlistObject(userArray, options);
    } else {
        return html`
            <h2>Nothing to show here right now</h2>
        `;
    }
}


function returnMessages(client, focussedchannel){
    let messages = focussedchannel.messages;
    if (!messages || messages.length < 5) {
        return focussedchannel.fetchMessages(10)
          .then(messages.array().sort(e=>e.createdTimestamp).map(message => elements.chat(client, message)));
    }
    return messages.array().sort( (one, two)=>one.createdTimestamp-two.createdTimestamp )
      .map(message => elements.chat(client, message));
}


let chatContent = (client, focussedChannel)=>{
    if (!focussedChannel) return; // generally shouldn't happen
    if (focussedChannel.constructor.name == "User") {
      focussedChannel=focussedChannel.dmChannel;
      if (!focussedChannel) {
        return html`
          <h1>
            You have selected a user DM, but since the client started, you have yet do contact each other.
            <br>
            Bot accounts have no access to message history, sadly.
          </h1>
        `;
      }
    }

    switch (focussedChannel.type) {
        case "text":
            if (!focussed.channel.permissionsFor( focussed.channel.guild.member(client.user) ).has("VIEW_CHANNEL")) {
                return html`
                    <h1>
                        You don't have the access rights to view this
                    </h1>
                `;
            }
        case 'dm':
            let messages = focussedChannel.messages;
            if (!messages || messages.length < 5) {
                return focussedChannel.fetchMessages(10).then(
                  ()=>returnMessages(client, focussedChannel)
                );
            }
            return returnMessages(client, focussedChannel);
            break;
        case "category":
        case "voice":
            return html`
                <h1>Currently selected channel is not of type TEXT</h1>
            `;
        default:
            if(focussedChannel.id == client.user.id){
              return html`<h1>No sense trying to message yourself, it doesn't work.</h1>`;
            }

            console.warn("ERROR with channel type: " + focussedChannel.type);
            return html`
                <h1>Currently selected ${focussedChannel.type} channel is not of type TEXT</h1>
            `;
            //nothing for now
            break;
    }
}


let channelEnum = ["category", "text", "voice"];
function sortChannels(one, two){
    if (one.type == two.type) {
        return one.position-two.position;
    } else {
        if (one.type) {
            return channelEnum.indexOf(one.type) - channelEnum.indexOf(two.type);
        }
    }
}

const listchannels = (channels, focussedchannel)=>{ //need to consider category channels
    if(!channels) return;
    let allChannels = channels.array().sort(sortChannels);
    let categoryChannels = allChannels.filter(chan=>chan.type=="category");
    let chanelsWithoutParent = allChannels.filter(chan=>chan.type!="category" && !chan.parentID);
    // categoryChannels
    return chanelsWithoutParent.map(orphan => elements.channel(orphan, focussedchannel))
        .concat(categoryChannels.map(categorychannel => elements.categoryChannel(categorychannel, focussedchannel)));
}


const chatTextField = (client, focussedChannelOrUser)=>html`
  <textarea id='chatinput' name='chatinput' wrap='soft'
  placeholder='Start typing!' @keydown=${e=>{
    if(e.keyCode === 13 && e.source.value!="\n") interaction.sendMessage(client, focussedChannelOrUser);
  }}></textarea>
`;


const inputTextArea = (client, focussedChannelOrUser)=>{
    if(!focussedChannelOrUser) return html`"loading, no user or channel selected, somehow"`;

    switch (focussedChannelOrUser.constructor.name) {
      case 'User':
        return chatTextField(client, focussedChannelOrUser);
        break;
      case 'TextChannel':
        if (focussedChannelOrUser.permissionsFor(focussedChannelOrUser.guild.member(client.user)).has("SEND_MESSAGES")){
          return chatTextField(client, focussedChannelOrUser);
        } else {
          return html`<em class="red center">I'm sorry, I can't let you do that dave...</em>
          <br> It seems you don't have the rights to access this channel.`;
        }
        break;
      default:
        return html`What? How? ${focussedChannelOrUser}`;
    }
}

const chat = (client, focussedChannelOrUser)=>html`
    <div class="chat">
        <div class="channel-title">
            ${focussedChannelOrUser?focussedChannelOrUser.name:"No channel selected? weird..."}
        </div>
        <div class="history" wrap="soft">
            <!-- this is the chat history -->
            ${chatContent(client, focussedChannelOrUser)}
        </div>
        <div class="input grid-container">
            ${inputTextArea(client, focussedChannelOrUser)}
        </div>
    </div>
`;


let menuItems = {
    "Status":[
        {
            "type": "button",
            "name": "test",
            "function": function(client, e){

            }
        }
    ]
};


const dropdown = (client, focussed)=>html`
    <div class="dropdown">
        <button type="button" class="key" name="button">test button</button>
        <div class="menu">
            <div class="dropdown">
                <button type="button" class="key" name="button">test button</button>
                <div class="menu">
                    <button type="button" class="key" name="button">test button</button>
                    <button type="button" class="key" name="button">test button</button>
                </div>
            <div class="dropdown">
        </div>

        <button type="button" class="key" @click=${e=>alert("test")}>not a menu</button>
    <div class="dropdown">
`;


const menu = (client, focussed)=>html`
    <nav>
        <button type="button" class="key" name="button">
          test button
        </button>
        <button type="button" class="key" id="logout" @click=${e=>client.destroy()}>
          Logout
        </button>
        <button type="button" class="key" id="logout" @click=${e=>client.generateInvite().then(alert)}>
          invite link
        </button>
        <button type="button" class="key" @click=${e=>{renderAll(client, focussed); console.warn("forced render");}}>
          force render
        </button>
        ${dropdown(client, focussed)}
    </nav>
`;

const main = (client, focussed)=>{
    if (focussed.guild && focussed.guild.id == "Friends and Users") {
        return html`
            <div class="grid-container">
                <div class="guilds" dropzone="move" ondrop="drop(event)"
                @dragover=${(event)=>interaction.allowDrop(event)}>
                    ${listguilds(client, client.guilds ? client.guilds : undefined, focussed.guild)}
                </div>
                ${userlist(client, focussed, sortUsersByLastMessage, {
                  click:(e, user)=>{focussed.user = user; renderAll(client, focussed);}
                })}
                ${chat(client, focussed.user)}
            </div>
        `;
    }

    return html`
        <div class="grid-container">
            <div class="guilds" dropzone="move" ondrop="drop(event)" @dragover=${(event)=>interaction.allowDrop(event)}>
                ${listguilds(client, client.guilds ? client.guilds : undefined, focussed.guild)}
            </div>
            <div class="" word-wrap="hard">
                ${listchannels(focussed.guild?focussed.guild.channels : undefined, focussed.channel||{})}
            </div>
            ${chat(client, focussed.channel)}
            ${userlist(client, focussed, false, {})}
        </div>
    `;
}

import login from "./login.js";
import {userContextMenu} from "./context.js";

const page = (client, focussed)=>html`
    ${login()}
    ${userContextMenu(client, focussed)}
    <div class="main">
        ${menu(client)}
        ${main(client, focussed)}
    </div>
`;

export default page;
