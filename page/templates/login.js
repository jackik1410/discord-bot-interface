import {html} from "https://unpkg.com/lit-html?module";

const login = ()=>html`


    <div class="loading">
        <img src="./external/crashcord.gif" alt="https://media1.tenor.com/images/83cdd1dd40cdb87020949e0f075b9648/tenor.gif">
    </div>

    <div class="login">
        <br><label>Please enter your credentials below:</label><br><br>
        <p>
            If you don't know what your token is, you may want to create a bot first. Visit <a href="https://discordapp.com/developers/applications/">the discord developer portal</a> to create an application and a bot, before returning and using your token here.
            <br>
            Test bot key: NjE5NzczODEzNDYyMjY5OTUz.XXqRsA.325XbGQJbuBUszKF1ah-1Ffywbc
        </p><br>
        <div class="grid-container">
            Enter Bot Token:<input type="text" id="token"></input>
            <button class="login submit key" id="login">login</button>
        </div>
    </div>
`;

export default login;
