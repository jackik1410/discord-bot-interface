import {html} from "https://unpkg.com/lit-html?module";

import * as interaction from "../interaction.js";

let defaultAvatar = "https://cdn.discordapp.com/embed/avatars/0.png";

import embed from "./embed.js";
import icon from "./SVGs.js";

let channelEnum = ["category", "text", "voice"];
function sortChannels(one, two){
    if (one.type == two.type) {
        return one.position-two.position;
    } else {
        if (one.type) {
            return channelEnum.indexOf(one.type) - channelEnum.indexOf(two.type);
        }
    }
}

let elements = {
    "chat": (client, msg)=>html`
        <div class="chat message">
            <div class="date">
                ${new Date(msg.createdTimestamp).toLocaleString() +"\n"}
            </div>
            ${elements.user((msg.member || msg.author), {
                class:"inline"
            })} ${msg.content} ${(msg.embeds && msg.embeds.map(embed))} ${msg.attachments}
        </div>
    `,
    "channel": (channel, focussedchannel)=>html`
        <div id="${channel.id}"  class="channel ${channel.type} ${channel.id==focussedchannel.id?" focussed":""} ${channel.notify?"alert":""}" @click=${e =>interaction.focusElement("channel", channel)}>
            ${channel.type=="text"?(icon.channel.notlocked.text):(icon.channel.notlocked.voice)}
            ${channel.name}
        </div>
    `,
    "categoryChannel": (channel, focussedchannel)=>{
        let children = channel.children.array().sort(sortChannels);
        return html`
            <div id="${channel.id}"  class="channel category${channel.id==focussedchannel.id?" focussed":""}${children.some(c => c.notify)?" alert":""}" @click=${e => {if(e.target.id == channel.id) e.target.classList.toggle("collapsed")}}>
                ${icon.channel.notlocked.category}
                ${channel.name}
                <div class="children">
                    ${children.map(child => elements.channel(child, focussedchannel))}
                </div>
            </div>
        `;
    },
    "server": (guild, focussedguild)=>html`
        <div id="${guild.id}" class="server${guild.id==focussedguild.id?" focussed":""}${guild.notify?" alert":""}"
        @click=${e =>interaction.focusElement("guild", guild)}
        title="test" draggable="true"
        @dragstart=${()=>interaction.test(event)}>
            <img src="${guild.iconURL || defaultAvatar}" alt="${defaultAvatar}">
            ${guild.name}
        </div>
    `,
    "user": (userOrMember, options)=>{
      // RegEx for a user reference:
      // /<@([0-9]{18})>/m
      return html`
          <div class="user ${userOrMember.presence.status} hascontext ${options.class||''}"
          @click=${(e)=> {
            if (options.click){
              options.click(e, userOrMember);
            } else interaction.showUserInfo(e, userOrMember.id||userOrMember.user.id);
          }}
          @contextmenu=${(e)=> {
            if(options.context) {
              options.context(e, userOrMember);
            } else interaction.showUserInfo(e, userOrMember.id||userOrMember.user.id);
          }}
          draggable="true"
          @dragstart=${(e)=> {
            if (options.drag) {
              options.drag(e, userOrMember);
            } else {
              // event.dataTransfer.setData("id", event.target.id);
              // event.dataTransfer.setData("user", userOrMember);
              // event.dataTransfer.setData("type", "user");
              e.dataTransfer.setData("application/json", {
                type: "user",
                content: userOrMember
              });
            }
          }}>
              <img src="${userOrMember.avatarURL || (userOrMember.user && userOrMember.user.avatarURL) || defaultAvatar}" alt="${defaultAvatar}">
              ${userOrMember.displayName || userOrMember.nickname || userOrMember.name || userOrMember.username || "something went wrong"}
          </div>
      `;
    }
};

export default elements;
