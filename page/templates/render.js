'use strict';
import {html, render} from "https://unpkg.com/lit-html?module";
import page from "./page.js";

const renderAll = (client, focussed)=>{
    render(page(client, focussed), document.body);
};


export {renderAll};
