import {html} from "https://unpkg.com/lit-html?module";


document.querySelector('img').addEventListener('contextmenu', function(e) {
		// Alternative
	    e.preventDefault();
	}, false);

let actionObject = {
    "test": (client, focussed)=>{return ""+focussed+ client},
    "ID": function(client, focussed){
        return focussed.object?focussed.object.id:"no id";
    },
    "name": function(client, focussed){
        return (focussed.object? (focussed.object.user?focussed.object.user.username:focussed.object.username) :"no name") || `undefined: ${focussed.object}`;
    },
    "account type": function(client, focussed){
        return focussed.object?(focussed.object.bot?"Bot":"User"):""
    },
    // "": function(client, focussed){
    //     return
    // },
    // "": function(client, focussed){
    //     return
    // },
    "simple text": "more text",
    "info":{"not so simple":"but still text"}
};

let action = (name, content, client, focussed) => html`
    <div class="dropdown action">
        <button class="contextkey">
            ${name}
        </button>
        <div class="menu">
            ${function (client, focussed, content){switch (typeof content){
                case "string":
                    return content;
                    break;
                case "object":
                    return userContextActions(content);
                    break;
                case "function":
                    return content(client, focussed);
                    break;
                default:
                    return "Neither string nor object" + (typeof content);
            }}(client, focussed, content)}
        </div>
    </div>
`;

let userContextActions = (objectToIterate, client, focussed)=>{
    return Object.keys(objectToIterate).map(key => action(key, actionObject[key], client, focussed));
}


const userContextMenu = (client, focussed)=> html`
        <div class="context hidden" @contextmenu=${e=>e.preventDefault()}>
            ${!focussed.object?"":userContextActions(actionObject, client, focussed)}
        </div>
`;

export {userContextMenu};
