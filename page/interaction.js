
export function test(event){
    console.log(event);
    console.log(event.srcElement);
}



//  default drag and drop stuff
export function allowDrop(event) {
    // event.prevententDefault();
}

export function drag(event) {
    event.dataTransfer.setData("id", event.target.id);
    console.log(event.target);
}

export function drop(event) {
    event.preventDefault();
    var data = event.dataTransfer.getData("id");
    console.log(event.dataTransfer);
    event.target.appendChild(document.getElementById(data));
}



import {renderAll} from "./templates/render.js";
export function focusElement(type, element){
    // console.log(type, element);

    // if (element) {
    //
    // }

    if (element.type && element.type=="text") {

        let historyElement = document.querySelector(".history");
        if (element.messages.size < 10) {
            console.log("not enough messages stored, fetching more!");
            element.fetchMessages(20)
              .then(()=>renderAll(client, focussed))
              .then(e=>{
                console.log("fetched 20 more messages");


                historyElement.scrollTop = historyElement.scrollHeight;//scroll after loading messages
              })
              .catch(console.error);
        } else {
            historyElement.scrollTop = historyElement.scrollHeight;
        }
    }

    // console.warn(focussed.guild.id);
    // console.warn(element.id);
    //
    if (type==="guild" && focussed.guild !== element) {//autoselect system channel if selected guild is switched
        // focusElement("channel", element.systemChannel)
        focussed["channel"] = element.systemChannel;
    }

    if (element.notify) {
        element.notify = false;
    }

    focussed[type] = element;
    renderAll(client, focussed);
}


export function showUserInfo(event, userid){
    if(event.preventDefault) event.preventDefault();
    // console.log("looking up user with id: "+userid);
    if (client.user.id == userid) {
        alert("You should know who you are!");
        return;
    }

    let contextMenu = document.querySelector(".context");

    contextMenu.style.top = event.pageY+"px";
    contextMenu.style.left = event.pageX+"px";
    if (!contextMenu) {
        console.warn("contextMenu doesn't exist");
    }

    focussed.object = {id:userid}; //probably not necessary, but just in case the fetch doesn't work perfectly
    client.fetchUser(userid).then(user => {
        // console.log(user);

        focussed.object = user;

        // alert(JSON.stringify(user));

        if (!contextMenu.classList.contains("hidden")) {
            //do stuff to make it close if clicked somewhere else or something
            //is handled in uistuff.js, as a window.onclick
        } else {
            contextMenu.classList.toggle("hidden");//shows it
            // focussed.object = false;
        }
        renderAll(client, focussed);//updates it
    }).catch(error => {
        alert("error with fetching user: "+error);
    });
}

export function sendMessage(client, focussedChannel){
    // event.keyCode == 13
    let input = document.querySelector("textarea#chatinput");
    focussedChannel.send(input.value);
    input.value = "";
}

export function fetchMessages(event){
    // let historyElement = event.srcElement;
    // console.log(event);
    if (event.srcElement.scrollTop < 50) {
        if (focussedChannel && focussedChannel.type == "text") {
            console.warn("fetching messages after scrolling");
            focussedChannel.fetchMessages(20).then(()=>{
              renderAll(client, focussed);
            });
        }
    }
    // console.log(historyElement.scrollTop);
}














var dragged;

/* events fired on the draggable target */
document.addEventListener("drag", function( event ) {

}, false);

document.addEventListener("dragstart", function( event ) {
    // store a ref. on the dragged elem
    dragged = event.target;
    // make it half transparent
    event.target.style.opacity = .5;
}, false);

document.addEventListener("drop", function( event ) {
    // prevent default action (open as link for some elements)
    event.preventDefault();
    // move dragged elem to the selected drop target
    if ( event.target.className == "dropzone" ) {
        event.target.style.background = "";
        dragged.parentNode.removeChild( dragged );
        event.target.appendChild( dragged );
    }

}, false);

// document.addEventListener("dragend", function( event ) {
//     // reset the transparency
//     event.target.style.opacity = "";
// }, false);

/* events fired on the drop targets */
// document.addEventListener("dragover", function( event ) {
//     // prevent default to allow drop
//     event.preventDefault();
// }, false);
//
// document.addEventListener("dragenter", function( event ) {
//     // highlight potential drop target when the draggable element enters it
//     if ( event.target.className == "dropzone" ) {
//         event.target.style.background = "purple";
//     }
//
// }, false);

// document.addEventListener("dragleave", function( event ) {
//     // reset background of potential drop target when the draggable element leaves it
//     if ( event.target.className == "dropzone" ) {
//         event.target.style.background = "";
//     }
//
// }, false);
